/// <reference types="node" />
import { Command } from './command';
import { IncomingMessage } from 'http';
import { Observable } from 'rxjs/Observable';
export declare class UrlMatcher {
    readonly commands: Array<Command>;
    constructor(commands: Array<Command>);
    match(request: IncomingMessage): Observable<Command>;
}
