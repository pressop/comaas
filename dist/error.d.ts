/// <reference types="node" />
import { ServerResponse } from 'http';
export declare class HttpError {
    private _message;
    private _code;
    constructor(_message?: string, _code?: number);
    readonly message: string;
    readonly code: number;
}
export declare function errorHandlerFactory(response: ServerResponse): (error: HttpError) => void;
