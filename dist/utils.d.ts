/// <reference types="node" />
import { Observable } from 'rxjs/Observable';
import { IncomingMessage } from 'http';
import { CommandRequest } from './command';
export declare function jsonRequestBody(request: IncomingMessage): Observable<CommandRequest>;
