/// <reference types="node" />
import { ServerResponse } from 'http';
import { Observable } from 'rxjs/Observable';
export declare class CommandContext {
    cmd: string;
    args: Array<Argument>;
    output: Buffer | string | null;
    after_exec: Observable<CommandContext>;
    before_exec: Observable<CommandContext>;
}
export interface Command {
    getBin(): string;
    getPath(): string;
    createContext(request: CommandRequest): Observable<CommandContext>;
    prepareResponse(response: ServerResponse, request: CommandRequest): void;
}
export interface Argument {
    key: string | null;
    value?: string | number | boolean | null;
    separator?: string;
}
export interface CommandRequest {
    args?: Array<Argument>;
}
export declare class ArgumentParser {
    static validator(args: Array<any>): Array<Argument>;
    static normalize(args: Array<Argument>): Array<Argument>;
    static stringify(args: Array<Argument>): string;
}
