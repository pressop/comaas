"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var http_1 = require("http");
var url_matcher_1 = require("./url-matcher");
var command_1 = require("./command");
var utils_1 = require("./utils");
var error_1 = require("./error");
var child_process_1 = require("child_process");
function createServer(commands) {
    var urlMatcher = new url_matcher_1.UrlMatcher(commands);
    return http_1.createServer(function (request, response) {
        var errorHandler = error_1.errorHandlerFactory(response);
        var bodyObservable = utils_1.jsonRequestBody(request);
        urlMatcher.match(request).subscribe(function (command) {
            bodyObservable.subscribe(function (body) {
                command.prepareResponse(response, body);
                command.createContext(body).subscribe(function (context) {
                    context.before_exec.subscribe(function (context) {
                        var cmd = context.cmd + ' ' + command_1.ArgumentParser.stringify(context.args);
                        child_process_1.exec(cmd, function (error, stdout) {
                            if (null !== error) {
                                errorHandler(new error_1.HttpError(error.message, 503));
                                return;
                            }
                            context.output = stdout;
                            context.after_exec.subscribe(function (context) {
                                if (context.output instanceof Buffer) {
                                    response.write(context.output, 'binary');
                                    response.end(null, 'binary');
                                }
                                else {
                                    response.write(context.output);
                                    response.end();
                                }
                            });
                        });
                    });
                }, errorHandler);
            }, errorHandler);
        }, errorHandler);
    });
}
exports.createServer = createServer;
