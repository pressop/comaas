"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Observable_1 = require("rxjs/Observable");
var error_1 = require("./error");
var UrlMatcher = /** @class */ (function () {
    function UrlMatcher(commands) {
        this.commands = commands;
    }
    UrlMatcher.prototype.match = function (request) {
        var _this = this;
        return Observable_1.Observable.create(function (observer) {
            for (var _i = 0, _a = _this.commands; _i < _a.length; _i++) {
                var command = _a[_i];
                if (request.url === command.getPath()) {
                    observer.next(command);
                    observer.complete();
                    return;
                }
            }
            observer.error(new error_1.HttpError('Command not found.', 404));
            observer.complete();
        });
    };
    return UrlMatcher;
}());
exports.UrlMatcher = UrlMatcher;
