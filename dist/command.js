"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Observable_1 = require("rxjs/Observable");
var error_1 = require("./error");
var CommandContext = /** @class */ (function () {
    function CommandContext() {
        var _this = this;
        this.args = [];
        this.output = null;
        this.after_exec = Observable_1.Observable.create(function (observer) {
            observer.next(_this);
            observer.complete();
        });
        this.before_exec = Observable_1.Observable.create(function (observer) {
            observer.next(_this);
            observer.complete();
        });
    }
    return CommandContext;
}());
exports.CommandContext = CommandContext;
var ArgumentParser = /** @class */ (function () {
    function ArgumentParser() {
    }
    ArgumentParser.validator = function (args) {
        var validArgs = [];
        for (var _i = 0, args_1 = args; _i < args_1.length; _i++) {
            var arg = args_1[_i];
            var realArg = {
                key: null,
                value: null,
                separator: null,
            };
            if (typeof arg === 'string') {
                realArg.key = arg;
            }
            else if (typeof arg === 'object') {
                if (!arg.hasOwnProperty('key')) {
                    throw new error_1.HttpError('Missing key for argument "' + JSON.stringify(arg) + '".', 400);
                }
                realArg.key = arg.key;
                if (arg.hasOwnProperty('value')) {
                    if (!(typeof arg.value === 'string' ||
                        typeof arg.value === 'number' ||
                        typeof arg.value === 'boolean' ||
                        null === arg.value)) {
                        throw new error_1.HttpError('The value option for "' + realArg.key + '" has to be a string, number, boolean or null.', 400);
                    }
                    realArg.value = arg.value;
                }
                if (arg.hasOwnProperty('separator')) {
                    if (typeof arg.separator !== 'string') {
                        throw new error_1.HttpError('The separator option for "' + realArg.key + '" has to be a string.', 400);
                    }
                    realArg.separator = arg.separator;
                }
            }
            else {
                throw new error_1.HttpError('Invalid arg definition for "' + JSON.stringify(arg) + '".', 400);
            }
            validArgs.push(realArg);
        }
        return validArgs;
    };
    ArgumentParser.normalize = function (args) {
        for (var _i = 0, args_2 = args; _i < args_2.length; _i++) {
            var arg = args_2[_i];
            if (!arg.hasOwnProperty('separator') || null === arg.separator) {
                arg.separator = ' ';
            }
            if (!arg.hasOwnProperty('value')) {
                arg.value = null;
            }
            if (typeof arg.value === 'string' && arg.value.length > 0) {
                arg.value = '"' + arg.value + '"';
            }
        }
        return args;
    };
    ArgumentParser.stringify = function (args) {
        var str = '';
        for (var _i = 0, args_3 = args; _i < args_3.length; _i++) {
            var arg = args_3[_i];
            str += ' ' + arg.key;
            if (null !== arg.value && true !== arg.value) {
                str += arg.separator + arg.value;
            }
        }
        return str;
    };
    return ArgumentParser;
}());
exports.ArgumentParser = ArgumentParser;
