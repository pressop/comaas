/// <reference types="node" />
import { Server } from 'http';
import { Command } from './command';
export declare function createServer(commands: Array<Command>): Server;
