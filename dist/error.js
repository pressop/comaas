"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var HttpError = /** @class */ (function () {
    function HttpError(_message, _code) {
        if (_message === void 0) { _message = 'Error'; }
        if (_code === void 0) { _code = 400; }
        this._message = _message;
        this._code = _code;
    }
    Object.defineProperty(HttpError.prototype, "message", {
        get: function () {
            return this._message;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(HttpError.prototype, "code", {
        get: function () {
            return this._code;
        },
        enumerable: true,
        configurable: true
    });
    return HttpError;
}());
exports.HttpError = HttpError;
function errorHandlerFactory(response) {
    return function (error) {
        response.setHeader('Content-Type', 'application/json');
        response.statusCode = error.code;
        response.write(JSON.stringify({
            'status_code': error.code,
            'message': error.message,
        }));
        response.end();
    };
}
exports.errorHandlerFactory = errorHandlerFactory;
