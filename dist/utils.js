"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Observable_1 = require("rxjs/Observable");
var command_1 = require("./command");
var error_1 = require("./error");
function jsonRequestBody(request) {
    return Observable_1.Observable.create(function (observer) {
        var body = '';
        if ('POST' !== request.method.toUpperCase()) {
            observer.error(new error_1.HttpError('POST only.', 405));
            observer.complete();
            return;
        }
        if (request.headers.hasOwnProperty('content-type') && 'application/json' === request.headers['content-type']) {
            request.on('readable', function () {
                var read = request.read();
                if (null !== read) {
                    body += read;
                }
            });
            request.on('end', function () {
                var data = {};
                try {
                    data = JSON.parse(body);
                }
                catch (e) { }
                if (!data.hasOwnProperty('args')) {
                    data.args = [];
                }
                try {
                    data.args = command_1.ArgumentParser.validator(data.args);
                    observer.next(data);
                }
                catch (e) {
                    observer.error(e);
                }
                observer.complete();
            });
        }
        else {
            observer.error(new error_1.HttpError('"application/json" only.', 400));
            observer.complete();
        }
    });
}
exports.jsonRequestBody = jsonRequestBody;
