import gulp from 'gulp';
import del from 'del';
import vinylPaths from 'vinyl-paths';

gulp.task('clean', () => {
    return gulp.src('dist')
        .pipe(vinylPaths(del))
    ;
});
