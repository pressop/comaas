import gulp from 'gulp';
import {createProject} from 'gulp-typescript';

let tsProject = createProject('tsconfig.json');

gulp.task('build', ['build-definition'], () => {
    return tsProject.src()
        .pipe(tsProject()).js
        .pipe(gulp.dest('dist'))
    ;
});

gulp.task('build-definition', () => {
    return tsProject.src()
        .pipe(tsProject()).dts
        .pipe(gulp.dest('dist'))
    ;
});
