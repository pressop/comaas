import gulp from 'gulp';

gulp.task('watch', ['build'], () => {
    gulp.watch('src/**/*.ts').on('change', (event) => {
        let cwd = process.cwd();
        let file = event.path;

        if (file.indexOf(cwd) !== -1) {
            file = file.replace(cwd, '').replace(/^[\\\/]/, '');
        }

        console.log('--------------------------------------------------------');
        console.log('  File ' + event.type + ': ' + file);
        console.log('--------------------------------------------------------');

        gulp.run('build');
    });
});