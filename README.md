COMmand As A Service
==================

Make command available as http service. Useful in micro services.

!!! Do not come with security system, you have to implement your own security layer. !!!

## Install
```bash
yarn add comaas
```

## An example with ls command (not relevant but easy to understand)

```typescript
import {
    createServer,
    ArgumentParser,
    Command,
    CommandContext,
    CommandRequest,
} from 'comaas';
import {ServerResponse} from 'http';
import {Observable} from 'rxjs/Observable';
import {Observer} from 'rxjs/Observer';

class Ls implements Command {
    getBin(): string {
        return '/bin/ls';
    }

    getPath(): string {
        return '/ls';
    }

    createContext(request: CommandRequest): Observable<CommandContext> {
        return Observable.create((observer: Observer<CommandContext>) => {
            const context = new CommandContext();

            context.cmd = this.getBin();
            context.args = ArgumentParser.normalize(request.args);

            observer.next(context);
            observer.complete();
        });
    }

    prepareResponse(response: ServerResponse): void {
        response.statusCode = 200;
        response.setHeader('Content-Type', 'text/plain');
    }
}

let server = createServer([
    new Ls(),
]);

server.listen(8156, '127.0.0.1', () => {
    console.log('Server started on http://127.0.0.1:8156');
});
```

Request

```
curl --request POST \
  --url http://127.0.0.1:8156/ls \
  --header 'content-type: application/json' \
  --data '{
    "args": [{
        "key": "/home/user"
    }]
}'
```
 
## Know issues
- Nothing (yet)

## TODO
- Unit tests.
- Implement whitelist for args.
- Implement an arg value validator.

## License

Release under MIT license.
