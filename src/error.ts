import {ServerResponse} from 'http';

export class HttpError {
    constructor(private _message: string = 'Error', private _code: number = 400) {}

    get message(): string {
        return this._message;
    }

    get code(): number {
        return this._code;
    }
}

export function errorHandlerFactory(response: ServerResponse) {
    return (error: HttpError) => {
        response.setHeader('Content-Type', 'application/json');
        response.statusCode = error.code;

        response.write(JSON.stringify({
            'status_code': error.code,
            'message': error.message,
        }));

        response.end();
    };
}
