import {Observable} from 'rxjs/Observable';
import {Observer} from 'rxjs/Observer';
import {IncomingMessage} from 'http';
import {ArgumentParser, CommandRequest} from './command';
import {HttpError} from './error';

export function jsonRequestBody(request: IncomingMessage): Observable<CommandRequest> {
    return Observable.create((observer: Observer<CommandRequest>) => {
        let body = '';

        if ('POST' !== request.method.toUpperCase()) {
            observer.error(new HttpError('POST only.', 405));
            observer.complete();
            return;
        }

        if (request.headers.hasOwnProperty('content-type') && 'application/json' === request.headers['content-type']) {
            request.on('readable', () => {
                const read = request.read();

                if (null !== read) {
                    body += read;
                }
            });

            request.on('end', () => {
                let data: CommandRequest = {};
                try {
                    data = JSON.parse(body);
                } catch (e) {}

                if (!data.hasOwnProperty('args')) {
                    data.args = [];
                }

                try {
                    data.args = ArgumentParser.validator(data.args);
                    observer.next(data);
                } catch (e) {
                    observer.error(e);
                }

                observer.complete();
            });
        } else {
            observer.error(new HttpError('"application/json" only.', 400));
            observer.complete();
        }
    });
}
