import {ServerResponse} from 'http';
import {Observable} from 'rxjs/Observable';
import {Observer} from 'rxjs/Observer';
import {HttpError} from './error';

export class CommandContext {
    cmd: string;
    args: Array<Argument> = [];
    output: Buffer|string|null = null;
    after_exec: Observable<CommandContext> = Observable.create((observer: Observer<CommandContext>) => {
        observer.next(this);
        observer.complete();
    });
    before_exec: Observable<CommandContext> = Observable.create((observer: Observer<CommandContext>) => {
        observer.next(this);
        observer.complete();
    });
}

export interface Command {
    getBin(): string;
    getPath(): string;
    createContext(request: CommandRequest): Observable<CommandContext>;
    prepareResponse(response: ServerResponse, request: CommandRequest): void;
}

export interface Argument {
    key: string|null;
    value?: string|number|boolean|null;
    separator?: string;
}

export interface CommandRequest {
    args?: Array<Argument>;
}

export class ArgumentParser {
    static validator(args: Array<any>): Array<Argument> {
        const validArgs: Array<Argument> = [];
        for (let arg of args) {
            const realArg: Argument = {
                key: null,
                value: null,
                separator: null,
            };

            if (typeof arg === 'string') {
                realArg.key = arg;
            } else if (typeof arg === 'object') {
                if (!arg.hasOwnProperty('key')) {
                    throw new HttpError('Missing key for argument "' + JSON.stringify(arg) + '".', 400);
                }

                realArg.key = arg.key;

                if (arg.hasOwnProperty('value')) {
                    if (!(
                        typeof arg.value === 'string' ||
                        typeof arg.value === 'number' ||
                        typeof arg.value === 'boolean' ||
                        null === arg.value
                    )) {
                        throw new HttpError(
                            'The value option for "' + realArg.key + '" has to be a string, number, boolean or null.',
                            400
                        );
                    }

                    realArg.value = arg.value;
                }

                if (arg.hasOwnProperty('separator')) {
                    if (typeof arg.separator !== 'string') {
                        throw new HttpError(
                            'The separator option for "' + realArg.key + '" has to be a string.',
                            400
                        );
                    }

                    realArg.separator = arg.separator;
                }
            } else {
                throw new HttpError(
                    'Invalid arg definition for "' + JSON.stringify(arg) + '".',
                    400
                );
            }

            validArgs.push(realArg);
        }

        return validArgs;
    }

    static normalize(args: Array<Argument>): Array<Argument> {
        for (let arg of args) {
            if (!arg.hasOwnProperty('separator') || null === arg.separator) {
                arg.separator = ' ';
            }

            if (!arg.hasOwnProperty('value')) {
                arg.value = null;
            }

            if (typeof arg.value === 'string' && arg.value.length > 0) {
                arg.value = '"' + arg.value + '"';
            }
        }

        return args;
    }

    static stringify(args: Array<Argument>): string {
        let str = '';
        for (let arg of args) {
            str += ' ' + arg.key;

            if (null !== arg.value && true !== arg.value) {
                str += arg.separator + arg.value;
            }
        }

        return str;
    }
}
