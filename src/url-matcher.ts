import {Command} from './command';
import {IncomingMessage} from 'http';
import {Observable} from 'rxjs/Observable';
import {Observer} from 'rxjs/Observer';
import {HttpError} from './error';

export class UrlMatcher {
    readonly commands: Array<Command>;

    constructor(commands: Array<Command>) {
        this.commands = commands;
    }

    match(request: IncomingMessage): Observable<Command> {
        return Observable.create((observer: Observer<Command>) => {
            for (const command of this.commands) {
                if (request.url === command.getPath()) {
                    observer.next(command);
                    observer.complete();
                    return;
                }
            }

            observer.error(new HttpError('Command not found.', 404));
            observer.complete();
        });
    }
}
