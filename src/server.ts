import {createServer as httpCreateServer, IncomingMessage, Server, ServerResponse} from 'http';
import {UrlMatcher} from './url-matcher';
import {ArgumentParser, Command, CommandContext, CommandRequest} from './command';
import {jsonRequestBody} from './utils';
import {errorHandlerFactory, HttpError} from './error';
import {exec} from 'child_process';

export function createServer(commands: Array<Command>): Server {
    const urlMatcher = new UrlMatcher(commands);

    return httpCreateServer((request: IncomingMessage, response: ServerResponse) => {
        const errorHandler = errorHandlerFactory(response);
        const bodyObservable = jsonRequestBody(request);

        urlMatcher.match(request).subscribe((command: Command) => {
            bodyObservable.subscribe((body: CommandRequest) => {
                command.prepareResponse(response, body);

                command.createContext(body).subscribe((context: CommandContext) => {
                    context.before_exec.subscribe((context: CommandContext) => {
                        const cmd = context.cmd + ' ' + ArgumentParser.stringify(context.args);

                        exec(cmd, (error: Error|null, stdout: string) => {
                            if (null !== error) {
                                errorHandler(new HttpError(error.message, 503));
                                return;
                            }

                            context.output = stdout;
                            context.after_exec.subscribe((context: CommandContext) => {
                                if (context.output instanceof Buffer) {
                                    response.write(context.output, 'binary');
                                    response.end(null, 'binary');
                                } else {
                                    response.write(context.output);
                                    response.end();
                                }
                            });
                        });
                    });
                }, errorHandler);
            }, errorHandler);
        }, errorHandler);
    });
}
